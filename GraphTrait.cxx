#include "GraphTrait.h"
#include <iostream>
#include <TStyle.h>

//====================================================================
int 
GraphFrame::MakeId() 
{
  static int id = 0;
  return id++;
}

//____________________________________________________________________
GraphFrame::GraphFrame(TGCompositeFrame*      p, 
		       unsigned int           addr, 
		       const char*            name,
		       TGNumberFormat::EStyle style, 
		       Int_t                  col, 
		       Double_t               conv,
		       Double_t               offs,
		       Double_t               min, 
		       Double_t               max)
  : TGVerticalFrame(p), 
    fTop(this),
    fPause(&fTop, "Pause", 32*addr+1),
    fClear(&fTop, "Clear", 32*addr+2),
    fEntry(&fTop, -1, 0, style, TGNumberFormat::kNEAAnyNumber, 
	   TGNumberFormat::kNELLimitMinMax, min, max), 
    fEmCanvas(0, this, 600, 150),
    // fCanvas(Form("c_%s",name), 600, 150, fEmCanvas.GetCanvasWindowId()), 
    fStart(0), // time(NULL)), 
    fLast(fStart),
    fConv(conv),
    fOffset(offs)
{
  // Add frames 
  fTop.AddFrame(&fEntry, new TGLayoutHints(kLHintsExpandX));
  fTop.AddFrame(&fPause);
  fTop.AddFrame(&fClear);
  AddFrame(&fTop, new TGLayoutHints(kLHintsExpandX));
  AddFrame(&fEmCanvas, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
  
  // Buttons stay down 
  fPause.AllowStayDown(kTRUE);
  fClear.AllowStayDown(kTRUE);
  
  // Size of field 
  Int_t logmax = Int_t(TMath::Log10(max)) + 1;
  fEntry.SetMaxLength((logmax > 1 ? logmax : 1));
  fEntry.SetEditDisabled();
  fEntry.SetEnabled(kFALSE);

  // Style
  gStyle->SetOptTitle(0);

  // Canvas 
  TCanvas* c = new TCanvas(Form("c_%s",name), 600, 150, 
			   fEmCanvas.GetCanvasWindowId());
  fEmCanvas.AdoptCanvas(c);
  fEmCanvas.SetAutoFit(kTRUE);
  c->cd();
  c->SetBorderMode(0);
  c->SetBorderSize(0);
  c->SetFillColor(0);
  c->SetBottomMargin(0.10);
  c->SetLeftMargin(0.10);
  c->SetTopMargin(0.05);
  c->SetRightMargin(0.05);
  
  // Graph
  c->cd();
  fGraph.SetName(Form("g_%s", name));
  fGraph.SetTitle(name);
  fGraph.SetLineColor(col);
  fGraph.SetMarkerColor(col);
  fGraph.SetMarkerStyle(20);
  fGraph.SetMarkerSize(1);
  fGraph.GetHistogram()->GetXaxis()->SetTimeDisplay(1);
  fGraph.GetHistogram()->GetXaxis()->SetTimeFormat("%H:%M:%S");
  fGraph.GetHistogram()->GetXaxis()->SetTitleFont(132);
  fGraph.GetHistogram()->GetXaxis()->SetLabelFont(132);
  fGraph.GetHistogram()->GetYaxis()->SetTitleFont(132);
  fGraph.GetHistogram()->GetYaxis()->SetLabelFont(132);
  fGraph.GetHistogram()->SetMaximum(1.05*max);
  fGraph.GetHistogram()->SetMinimum(0.95*min);
  fGraph.GetHistogram()->GetXaxis()->SetTitle("t [s]");
  fGraph.GetHistogram()->GetYaxis()->SetTitle(name);
  fGraph.GetHistogram()->SetStats(0);
  fGraph.Draw("apl");
}



//____________________________________________________________________
void 
GraphFrame::Update(int t, Double_t v)
{
  // Get the first time
  if (fStart == 0) fStart = t;
  
  // Check if we're clearing 
  if (fClear.IsDown()) { 
    fClear.SetDown(kFALSE);
    int n = fGraph.GetN();
    for (int i = n-1; i >= 0; i--) fGraph.RemovePoint(i);
  }

  // Calculate the real value 
  double rv = fConv * v + fOffset;

  // If pause is down, just return. 
  if (fPause.IsDown()) return;

  // Update the last value
  fLast = rv;

  // Set the number display 
  fEntry.SetNumber(rv);

  // Update the graph
  TCanvas* c = fEmCanvas.GetCanvas();
  c->cd();
  Int_t i  = fGraph.GetN();
  Int_t tt = t;
  fGraph.SetPoint(i, tt, rv);

  // Reset axis range 
  if (tt > fStart) {
    int nbins = TMath::Max(20,TMath::Min(3,i));
    fGraph.GetHistogram()->SetBins(nbins, fStart, tt);
  }

  // refresh the canvas 
  c->Modified();
  c->Update();
  c->cd();
}

//====================================================================
void*    
IntGraphTrait::Address(const int& x) 
{ 
  return reinterpret_cast<void*>(&(const_cast<int&>(x))); 
}

//____________________________________________________________________
int
IntGraphTrait::Compare(int x, int y, const int& tol) 
{ 
  return TMath::Abs(x-y) < tol;
}

//____________________________________________________________________
TGFrame* 
IntGraphTrait::MakeFrame(TGCompositeFrame* p, 
			 unsigned int      addr, 
			 const char*       name, 
			 int               color,
			 double            conversion,
			 double            offset,
			 int               min,
			 int               max) 
{
  return new GraphFrame(p, addr, name, 
			(conversion == 1 ? 
			 TGNumberFormat::kNESInteger :
			 TGNumberFormat::kNESReal),
			color, conversion, offset, min, max);
}
//____________________________________________________________________
void     
IntGraphTrait::UpdateFrame(TGFrame* f, DimInfo& i) 
{ 
  GraphFrame* d = dynamic_cast<GraphFrame*>(f);
  if (!d) { 
    std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
    return;
  }
  d->Update(i.getTimestamp(), Value(i));
}

//====================================================================
void*    
FloatGraphTrait::Address(const float& x) 
{ 
  return reinterpret_cast<void*>(&(const_cast<float&>(x))); 
}
//____________________________________________________________________
float
FloatGraphTrait::Compare(float x, float y, const float& tol) 
{ 
  return TMath::Abs(x-y) < tol;
}
//____________________________________________________________________
TGFrame* 
FloatGraphTrait::MakeFrame(TGCompositeFrame* p, 
			   unsigned int      addr, 
			   const char*       name,
			   int               color,
			   double            conversion, 
			   double            offset,
			   float             min,
			   float             max) 
{
  return new GraphFrame(p, addr, name, TGNumberFormat::kNESReal, color,
			conversion, offset, min, max);
}
//____________________________________________________________________
void
FloatGraphTrait::UpdateFrame(TGFrame* f, DimInfo& i) 
{ 
  GraphFrame* d = dynamic_cast<GraphFrame*>(f);
  if (!d) { 
    std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
    return;
  }
  d->Update(i.getTimestamp(), Value(i));
}



//====================================================================
int 
MultiGraphFrame::MakeId() 
{
  static int id = 0;
  return id++;
}

//____________________________________________________________________
MultiGraphFrame::MultiGraphFrame(TGCompositeFrame*      p, 
				 unsigned int           addr)
  : TGVerticalFrame(p), 
    fTop(this),
    fPause(&fTop, "Pause", 32*addr+1),
    fClear(&fTop, "Clear", 32*addr+2),
    fEmCanvas(0, this, 600, 400),
    fCanvas(Form("c_%02d",addr), 600, 150, fEmCanvas.GetCanvasWindowId()), 
    fGraphs(),
    fStarts(),
    fLasts(),
    fConvs(),
    fOffsets()
{
  // Add frames 
  fTop.AddFrame(&fPause);
  fTop.AddFrame(&fClear);
  AddFrame(&fTop, new TGLayoutHints(kLHintsExpandX));
  AddFrame(&fEmCanvas, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
  
  // Buttons stay down 
  fPause.AllowStayDown(kTRUE);
  fClear.AllowStayDown(kTRUE);
  
  // Style
  gStyle->SetOptTitle(0);

  // Canvas 
  fEmCanvas.AdoptCanvas(&fCanvas);
  fEmCanvas.SetAutoFit(kTRUE);
  fCanvas.cd();
  fCanvas.SetBorderMode(0);
  fCanvas.SetBorderSize(0);
  fCanvas.SetFillColor(0);
  fCanvas.SetBottomMargin(0.10);
  fCanvas.SetLeftMargin(0.10);
  fCanvas.SetTopMargin(0.05);
  fCanvas.SetRightMargin(0.05);
}

//____________________________________________________________________
void
MultiGraphFrame::AddService(const char* name, int col, 
			    double conv, double off, 
			    double min,  double max)
{
  std::string n(name);
  GraphMap::iterator i = fGraphs.find(n);
  if (i != fGraphs.end()) { 
    std::cerr << "Already have graph for " << name << std::endl;
    return;
  }
  TGraph* g = fGraphs[n]   = new TGraph(0);
  fConvs[n]    = conv;
  fOffsets[n]  = off;
  fLasts[n]    = 0;
  fStarts[n]   = 0;

  // Graph
  fCanvas.cd();
  g->SetName(Form("g_%s", name));
  g->SetTitle(name);
  g->SetLineColor(col);
  g->SetMarkerColor(col);
  g->SetMarkerStyle(20);
  g->SetMarkerSize(1);
  g->GetHistogram()->GetXaxis()->SetTimeDisplay(1);
  g->GetHistogram()->GetXaxis()->SetTimeFormat("%H:%M:%S");
  g->GetHistogram()->GetXaxis()->SetTitleFont(132);
  g->GetHistogram()->GetXaxis()->SetLabelFont(132);
  g->GetHistogram()->GetYaxis()->SetTitleFont(132);
  g->GetHistogram()->GetYaxis()->SetLabelFont(132);
  g->GetHistogram()->SetMaximum(1.05*max);
  g->GetHistogram()->SetMinimum(0.95*min);
  g->GetHistogram()->GetXaxis()->SetTitle("t [s]");
  g->GetHistogram()->GetYaxis()->SetTitle(name);
  g->GetHistogram()->SetStats(0);
  g->Draw(fGraphs.size() == 1 ? "apl" : "pl");
}



//____________________________________________________________________
void 
MultiGraphFrame::Update(const std::string& name, int t, Double_t v)
{
  // Get the first time
  GraphMap::iterator search = fGraphs.find(name);
  if (search == fGraphs.end()) return;
  TGraph* graph = search->second;

  if (fStarts[name] == 0) fStarts[name] = t;
  

  // Check if we're clearing 
  if (fClear.IsDown()) { 
    fClear.SetDown(kFALSE);
    int n = graph->GetN();
    for (int i = n-1; i >= 0; i--) graph->RemovePoint(i);
  }

  // Calculate the real value 
  double rv = fConvs[name] * v + fOffsets[name];

  // If pause is down, just return. 
  if (fPause.IsDown()) return;

  // Update the last value
  fLasts[name] = rv;

  // Update the graph
  fCanvas.cd();
  Int_t i  = graph->GetN();
  Int_t tt = t;
  graph->SetPoint(i, tt, rv);

  // Reset axis range 
  if (tt > fStarts[name]) {
    int nbins = TMath::Max(20,TMath::Min(3,i));
    graph->GetHistogram()->SetBins(nbins, fStarts[name], tt);
  }
  
  // refresh the canvas 
  fCanvas.Modified();
  fCanvas.Update();
  fCanvas.cd();
}
//____________________________________________________________________
//
// EOF
//


// -*- mode: C++ -*-
#ifndef MAIN_H
#define MAIN_H
#include <TGFrame.h>
#include <TGTab.h>
#include <string>
#include <vector>
#include "Fec.h"
#include "Traits.h"
#include "InfoDisplay.h"
struct Main;

//____________________________________________________________________
struct Afl : public InfoDisplay<int, IntTrait>
{
  typedef   InfoDisplay<int,IntTrait> Base_t;
  /** Constructor */
  Afl(TGCompositeFrame* p, Main* m, const char* name)
    : Base_t(p, 0, name, -1, 1, 1, 0, 0xFFFFFFFF, kBlack, 1, 0),
      fMain(m)
  {}
  /** Set no link */
  void SetNoLink();
  /** Set updated */
  void SetUpdated();
  /** Set OK */
  void SetOk();
  /** Reference to main */
  Main* fMain;
};

//____________________________________________________________________
struct Init
{
  Init(const char* dns);
};

//____________________________________________________________________
/** Main frame */
class Main : public TGMainFrame, public Init, public DimErrorHandler
{
public:
  /** Constructor 
      @param dns DIM Domain name server 
      @param name NAme of Server */
  Main(const char* dns, const char* name); 
  /** Terminate */
  void CloseWindow();
  /** Show it */
  void Display();
  /** Handle updates of AFL */
  void HandleAFL(int afl);
  /** Handle change of size */
  Bool_t HandleConfigureNotify(Event_t *event);
protected:
  /** Type of integer display */
  typedef InfoDisplay<int,IntTrait> IntDisplay ;
  /** Name */
  std::string                    fName;
  /** Container of fecs */
  std::vector<Fec*>              fFecs;
  /** Container of frames */
  std::vector<TGCompositeFrame*> fFrames;
  /** Layout hints */
  TGLayoutHints                  fLayouts;
  /** Tabs */
  TGTab                          fTabs;
  /** First frame */
  TGCompositeFrame*              fFirst;
  /** Front page */
  TGVerticalFrame                fFront;
  /** Main state */
  IntDisplay                     fMainState;
  /** RCU state */
  IntDisplay                     fRcuState;
  /** AFS */
  Afl                            fAFL;
  /** Error and status */
  IntDisplay                     fERRST;
  /** add a front-end card */
  void AddFec(unsigned int addr);
  /** Remove a front-end card */
  void RemoveFec(unsigned int addr);
  /** Handle dim errors */
  void errorHandler(int severity, int code, char *msg);
};


#endif
//
// EOF
//

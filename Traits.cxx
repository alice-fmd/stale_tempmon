#include "Traits.h"
#include <TGNumberEntry.h>
#include <TGButton.h>
#include <iostream>
#include <limits>

//====================================================================
void*
IntTrait::Address(const int& x) 
{ 
  return reinterpret_cast<void*>(&(const_cast<int&>(x))); 
}
//____________________________________________________________________
int    
IntTrait::Compare(int x, int y, const int& tol) 
{ 
  return TMath::Abs(x-y) < tol;
}
//____________________________________________________________________
TGFrame* 
IntTrait::MakeFrame(TGCompositeFrame* p, 
		    unsigned int      addr, 
		    const char*       name, 
		    int               color,
		    double            /*conversion*/,
		    double            /*offset*/,
		    int               min, 
		    int               max) 
{
  TGNumberEntryField* f 
    = new TGNumberEntryField(p, -1, 0, 
			     TGNumberFormat::kNESInteger, 
			     TGNumberFormat::kNEAAnyNumber, 
			     TGNumberFormat::kNELLimitMinMax,
			     min, max);
  f->SetTextColor(color);
  return f;
}
//____________________________________________________________________
void
IntTrait::UpdateFrame(TGFrame* f, DimInfo& i) 
{ 
  TGNumberEntryField* d = dynamic_cast<TGNumberEntryField*>(f);
  if (!d) { 
    std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
    return;
  }
  d->SetIntNumber(Value(i));
}


//====================================================================
void*
FloatTrait::Address(const float& x) 
{ 
  return reinterpret_cast<void*>(&(const_cast<float&>(x))); 
}
//____________________________________________________________________
float
FloatTrait::Compare(float x, float y, const float& tol) 
{ 
  return TMath::Abs(x-y) < tol;
}
//____________________________________________________________________
TGFrame* 
FloatTrait::MakeFrame(TGCompositeFrame* p, 
		      unsigned int      addr, 
		      const char*       name, 
		      int               color,
		      double            /*conversion*/,
		      double            /*offset*/,
		      float             min, 
		      float             max)
{
  TGNumberEntryField* f = 
    new TGNumberEntryField(p, -1, 0, 
			   TGNumberFormat::kNESReal, 
			   TGNumberFormat::kNEAAnyNumber, 
			   TGNumberFormat::kNELLimitMinMax, min, max); 
  f->SetTextColor(color);
  return f;
}

//____________________________________________________________________
void
FloatTrait::UpdateFrame(TGFrame* f, DimInfo& i) 
{ 
  TGNumberEntryField* d = dynamic_cast<TGNumberEntryField*>(f);
  if (!d) { 
    std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
    return;
  }
  d->SetNumber(Value(i));
}

//====================================================================
void*
BoolTrait::Address(const bool& x) 
{ 
  return reinterpret_cast<void*>(&(const_cast<bool&>(x))); 
}
//____________________________________________________________________
bool
BoolTrait::Value(DimInfo& info) 
{ 
  void* data = info.getData();
  bool  val  = *(reinterpret_cast<bool*>(data));
  return val;
}
//____________________________________________________________________
TGFrame* 
BoolTrait::MakeFrame(TGCompositeFrame* p, 
		     unsigned int      addr, 
		     const char*       name, 
		     int               color,
		     double            /*conversion*/,
		     double            /*offset*/,
		     bool              /*min*/, 
		     bool              /*max*/)
{
  TGTextButton* f = new TGTextButton(p, "");
  f->ChangeBackground(color);
}
//____________________________________________________________________
void
BoolTrait::UpdateFrame(TGFrame* f, DimInfo& i) 
{ 
  TGTextButton* d = dynamic_cast<TGTextButton*>(f);
  if (!d) { 
    std::cerr << "Frame is not a TGTextButton!" << std::endl;
    return;
  }
  d->ChangeBackground(Value(i) ? 0x00ff00 : 0xff0000);
}
//____________________________________________________________________
//
// EOF
//

#ifndef TEMPMON_LOCK_H
#define TEMPMON_LOCK_H


/** @struct Mutex 
    @brief Abstract interface to mutex locking */
struct Mutex
{
  virtual ~Mutex()  {}
  virtual int Lock() = 0;
  virtual int Unlock() = 0;
  static Mutex* Create();
};

/** The dim lock. */
struct DimMutex : public Mutex
{
  static DimMutex& Instance();
  int Lock();
  int Unlock();
 private:
  DimMutex() {}
  static DimMutex* fgInstance;
};

struct LockGuard 
{
  LockGuard(Mutex& l)  : fMutex(l) { fMutex.Lock(); }
  ~LockGuard() { fMutex.Unlock(); }
protected:
  Mutex& fMutex;
};
    
#endif

  

  

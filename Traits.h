// -*- mode: C++ -*-
#ifndef TRAITS_H
#define TRAITS_H
#include <cctype>
#include <dic.hxx>
class TGFrame;
class TGCompositeFrame;

//____________________________________________________________________
/** Trait that shows an integer */
struct IntTrait 
{
  /** GEt address of @param x */
  static void*    Address(const int& x);
  /** Get size of x */
  static size_t   Size(int x) { return sizeof(x); }
  /** Get value of service */
  static int      Value(DimInfo& info) { return info.getInt(); }
  /** Compare x to y */
  static int      Compare(int x, int y, const int& tol);
  /** Make GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, 
			    unsigned int      addr, 
			    const char*       name,
			    int               color,
			    double            conversion, 
			    double            offset,
			    int               min,
			    int               max);
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i);
};

//____________________________________________________________________
struct FloatTrait 
{
  /** GEt address of @param x */
  static void*    Address(const float& x);
  /** Get size of x */
  static size_t   Size(float x) { return sizeof(x); }
  /** Get value of service */
  static float    Value(DimInfo& info) { return info.getFloat(); }
  /** Compare x to y */
  static float    Compare(float x, float y, const float& tol);
  /** Make GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, 
			    unsigned int      addr, 
			    const char*       name,
			    int               color,
			    double            conversion, 
			    double            offset,
			    float             min,
			    float             max);
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i);
};

//____________________________________________________________________
struct BoolTrait 
{
  /** GEt address of @param x */
  static void*    Address(const bool& x);
  /** Get size of x */
  static size_t   Size(bool x) { return sizeof(x); }
  /** Get value of service */
  static bool     Value(DimInfo& info);
  /** Compare x to y */
  static bool     Compare(bool x, bool y, const bool&) { return x == y; }
  /** Mak GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, 
			    unsigned int      addr, 
			    const char*       name,
			    int               color,
			    double            conversion, 
			    double            offset,
			    bool              min,
			    bool              max);
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i);
};

#endif
//
// EOF
//



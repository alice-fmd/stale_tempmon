// -*- mode: C++ -*-
#ifndef GRAPH_TRAITS_H
#define GRAPH_TRAITS_H
#include <dim/dic.hxx>
#include <TGNumberEntry.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGButton.h>
#include <TH1.h>
#include <TMath.h>
#include <limits>
#include <map>

//____________________________________________________________________
/** Frame that shows the service listen to in frame, and shows a trend
    of the service in a canvas */
struct GraphFrame : public TGVerticalFrame
{
  /** Static function to make unique id's for the canvases */
  static int MakeId(); 
  /** Constructor. 
      @param p     Parent frame 
      @param name  Name of service 
      @param style Type of number 
      @param min   Minimum value 
      @param max   Maximum value */
  GraphFrame(TGCompositeFrame*      p, 
	     unsigned int           addr, 
	     const char*            name,
	     TGNumberFormat::EStyle style, 
	     Int_t                  col=kRed, 
	     Double_t               conv=1,
	     Double_t               offset=0,
	     Double_t               min=0, 
	     Double_t               max=INT_MAX);
  /** Update the value. 
      @param t Time since epoch 
      @param v New value */
  void Update(int t, Double_t v);
  /** Top frame */
  TGHorizontalFrame  fTop;
  /** Button */
  TGTextButton       fPause;
  /** Button */
  TGTextButton       fClear;
  /** View */
  TGNumberEntryField fEntry;
  /** Canvas */
  TRootEmbeddedCanvas fEmCanvas;
  /** Canvas */
  // TCanvas fCanvas;
  /** Graph */
  TGraph fGraph;
  /** Start time */
  int fStart;
  /** Last update */
  double fLast;
  /** Conversion factor */
  double fConv;
  /** Conversion offset */
  double fOffset;
};
  
//____________________________________________________________________
/** Trait that shows the number in a field and in a graph */
struct IntGraphTrait 
{
  /** GEt address of @param x */
  static void*    Address(const int& x); 
  /** Get size of x */
  static size_t   Size(int x) { return sizeof(x); }
  /** Get value of service */
  static int      Value(DimInfo& info) { return info.getInt(); }
  /** Compare x to y */
  static int      Compare(int x, int y, const int& tol);
  /** Mak GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, 
			    unsigned int      addr, 
			    const char*       name,
			    int               color,
			    double            conversion, 
			    double            offset,
			    int               min,
			    int               max);
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i);
};
//____________________________________________________________________
struct FloatGraphTrait 
{
  /** GEt address of @param x */
  static void*    Address(const float& x); 
  /** Get size of x */
  static size_t   Size(float x) { return sizeof(x); }
  /** Get value of service */
  static float      Value(DimInfo& info) { return info.getFloat(); }
  /** Compare x to y */
  static float      Compare(float x, float y, const float& tol);
  /** Mak GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, 
			    unsigned int      addr, 
			    const char*       name,
			    int               color,
			    double            conversion, 
			    double            offset,
			    float             min,
			    float             max);
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i);
};

//____________________________________________________________________
/** Frame that shows the service listen to in frame, and shows a trend
    of the service in a canvas */
struct MultiGraphFrame : public TGVerticalFrame
{
  /** Static function to make unique id's for the canvases */
  static int MakeId(); 
  /** Constructor. 
      @param p     Parent frame 
      @param name  Name of service 
      @param style Type of number 
      @param min   Minimum value 
      @param max   Maximum value */
  MultiGraphFrame(TGCompositeFrame*      p, 
		  unsigned int           addr);
  /** Update the value. 
      @param t Time since epoch 
      @param v New value */
  void Update(const std::string& name, int t, Double_t v);
  /** Add a service */
  void AddService(const char* name, int col, double conv, double off,
		  double min, double max);
  /** Top frame */
  TGHorizontalFrame  fTop;
  /** Button */
  TGTextButton       fPause;
  /** Button */
  TGTextButton       fClear;
  /** Canvas */
  TRootEmbeddedCanvas fEmCanvas;
  /** Canvas */
  TCanvas fCanvas;
  typedef std::map<std::string,TGraph*> GraphMap;
  typedef std::map<std::string,int> IntMap;
  typedef std::map<std::string,double> DoubleMap;
  /** Graph */
  GraphMap fGraphs;
  /** Start time */
  IntMap fStarts;
  /** Last update */
  DoubleMap fLasts;
  /** Conversion factor */
  DoubleMap fConvs;
  /** Conversion offset */
  DoubleMap fOffsets;
};

#endif

// -*- mode: C++ -*-
#ifndef __FEC_HH
#define __FEC_HH
#include <TGFrame.h>
#include "GraphTrait.h"
#include "Traits.h"
#include "InfoDisplay.h"
#define FEC_ALL_SERVICES 1

class Fec : public TGVerticalFrame
{
public:
  /** Constructor */
  Fec(const char* name, unsigned int addr, TGCompositeFrame* p);
  /** FEC address */
  unsigned int    fAddress;
  /** Scrolled view */
  TGCanvas        fCanvas;
  /** Container in scrolled view */
  TGVerticalFrame fCont;
  /** Configiration and status register 1*/
  InfoDisplay<int,IntTrait> fCSR1;
  /** Temperature monitor 1 */
  InfoDisplay<int,IntGraphTrait> fT1;
#if 1
  InfoDisplay<int,IntGraphTrait> fFLASH_I;
  InfoDisplay<int,IntGraphTrait> fAL_DIG_I;
  InfoDisplay<int,IntGraphTrait> fAL_ANA_I;
  InfoDisplay<int,IntGraphTrait> fVA_REC_IP;
  InfoDisplay<int,IntGraphTrait> fT2;
  InfoDisplay<int,IntGraphTrait> fVA_SUP_IP;
  InfoDisplay<int,IntGraphTrait> fVA_REC_IM;
  InfoDisplay<int,IntGraphTrait> fVA_SUP_IM;
  InfoDisplay<int,IntGraphTrait> fGTL_U;
  InfoDisplay<int,IntGraphTrait> fT3;
  InfoDisplay<int,IntGraphTrait> fT1_SENS;
  InfoDisplay<int,IntGraphTrait> fT2_SENS;
  InfoDisplay<int,IntGraphTrait> fAL_DIG_U;
  InfoDisplay<int,IntGraphTrait> fAL_ANA_U;
  InfoDisplay<int,IntGraphTrait> fT4;
  InfoDisplay<int,IntGraphTrait> fVA_REC_UP;
  InfoDisplay<int,IntGraphTrait> fVA_SUP_UP;
  InfoDisplay<int,IntGraphTrait> fVA_SUP_UM;
  InfoDisplay<int,IntGraphTrait> fVA_REC_UM;
#endif
  /** Layout hints */
  TGLayoutHints fHints;
};

#endif
//
// EOF
//

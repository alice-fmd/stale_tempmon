// -*- mode: C++ -*-
#ifndef INFO_DISPLAY_H
#define INFO_DISPLAY_H
#include <TGFrame.h>
#include <TGLabel.h>
#include <dic.hxx>
#include <limits>
#include "Lock.h"

//____________________________________________________________________
/** BAse class for service display */
class InfoDisplayBase : public TGGroupFrame
{
public:
  /** Static busy flag */
  static bool fBusy;
  /** Constructor 
      @param p Parent 
      @param name Name */
  InfoDisplayBase(TGCompositeFrame* p, 
		  unsigned int addr, 
		  const char* name);
protected:
  /** Set status display */
  void SetStatus(const char* what);
  /** Set status display to "No link" */
  virtual void SetNoLink()  { SetStatus("No link");  } 
  /** Set status display to "Updated" */
  virtual void SetUpdated() { SetStatus("Updated"); }
  /** Set status display to "OK" */
  virtual void SetOk()      { SetStatus("OK"); }
  /** Name of info */
  std::string  fName;
  /** Address of fec/RCU */
  unsigned int fAddress;
  /** Parent */
  TGFrame*     fParent;
};

//____________________________________________________________________
/** Class template for showing a service */
template <typename T, typename Trait>
class InfoDisplay : public InfoDisplayBase, public DimInfoHandler 
{
public:
  /** Constructor 
      @param p PArent 
      @param name Name 
      @param nolink Value on no link 
      @param timeout Timeout (if 0, never time out) */
  InfoDisplay(TGCompositeFrame* p, 
	      unsigned int addr, 
	      const char*  name, 
	      const T&     nolink, 
	      int          timeout, 
	      const T&     tolerance, 
	      const T&     min=std::numeric_limits<T>::min(), 
	      const T&     max=std::numeric_limits<T>::max(),
	      int          color=1,
	      double       conversion=1,
	      double       offset=0)
    : InfoDisplayBase(p, addr, name), 
      fNoLink(nolink),
      fTolerance(tolerance),
      fInfo(name, timeout, Trait::Address(nolink), Trait::Size(nolink), this), 
      fFrame(Trait::MakeFrame(this, addr, name, color, 
			      conversion, offset, min, max))
      
  {
    AddFrame(fFrame, new TGLayoutHints(kLHintsExpandX, 3, 3, 3, 3));
    fBusy = false;
  }
  /** Get the frame */
  TGFrame* Frame() { return fFrame; }
  /** HAndle updtes */
  virtual void infoHandler()
  {
    LockGuard g(DimMutex::Instance());
    if (fBusy) return;
    fBusy = true;
    T val = Trait::Value(fInfo);
    if (!Trait::Compare(val, fValue, fTolerance))
      Trait::UpdateFrame(fFrame, fInfo);
    if      (Trait::Compare(fNoLink, val, fTolerance)) SetNoLink();
    else if (Trait::Compare(val, fValue, fTolerance))  SetOk();
    else                                               SetUpdated();
    fValue = val;
    fBusy = false;
  }
protected:
  DimStampedInfo fInfo;
  // DimInfo  fInfo;
  const T  fNoLink;
  const T  fTolerance;
  T        fValue;
  TGFrame* fFrame;
};




#endif
//
// EOF
//

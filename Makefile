#
#
#
PACKAGE		= tempmon
VERSION		= 0.1

DIM_CPPFLAGS	= $(shell dim-config --cppflags)
DIM_CFLAGS	= $(shell dim-config --cflags)
DIM_LDFLAGS	= $(shell dim-config --ldflags)
DIM_LIBS	= /usr/lib/libdim.a -pthread
#DIM_LIBS	= $(shell dim-config --libs) -pthread
ROOT_CFLAGS	= $(shell root-config --cflags)
ROOT_LIBS	= $(shell root-config --glibs)

CLIENT_SOURCES	= tempmon.cxx		\
		  Main.cxx 		\
		  Lock.cxx		\
		  InfoDisplay.cxx 	\
		  Traits.cxx 		\
		  GraphTrait.cxx 	\
		  Fec.cxx
EXTRA_DIST	= README Makefile Doxyfile

CPP		= g++ -E
CPPFLAGS	= $(DIM_CPPFLAGS)
CXX		= g++ -c 
CXXFLAGS	= -g $(DIM_CFLAGS) $(ROOT_CFLAGS)
LD		= g++ 
LDFLAGS		= $(DIM_LDFLAGS)
LIBS		= $(DIM_LIBS) $(ROOT_LIBS)

%.o:%.cxx
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< 

%:%.o
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

%.cxx:%.h

all:	$(PACKAGE)

clean:
	rm -rf *.o $(PACKAGE)

dist:
	mkdir -p $(PACKAGE)-$(VERSION)
	cp $(CLIENT_SOURCES) $(SERVER_SOURCES) $(EXTRA_DIST) \
		$(PACKAGE)-$(VERSION)
	tar -cvzf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	rm -rf $(PACKAGE)-$(VERSION)

distcheck:dist
	tar -xzvf $(PACKAGE)=$(VERSION).tar.gz 
	(cd $(PACKAGE)-$(VERSION) && make) 
	rm -rf $(PACKAGE)-$(VERSION)

$(PACKAGE): $(CLIENT_SOURCES:%.cxx=%.o)

#
# EOF
#

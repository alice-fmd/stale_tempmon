#include "Main.h"
#include <iostream>
#include <TApplication.h>
#include <TSystem.h>
#include <dic.h>
#include "Lock.h"


//====================================================================
void 
Afl::SetNoLink()
{
  Base_t::SetNoLink();
  InfoDisplayBase::fBusy = false;
  fMain->HandleAFL(0);
  InfoDisplayBase::fBusy = true;
}

//____________________________________________________________________
void 
Afl::SetOk()
{
  Base_t::SetOk();
  static int counts = 0;
  counts++;
  if ((counts++) % 30 == 1)  {
    InfoDisplayBase::fBusy = false;
    fMain->HandleAFL(fValue);
    InfoDisplayBase::fBusy = true;
  }
}

//____________________________________________________________________
void 
Afl::SetUpdated()
{
  Base_t::SetNoLink();
  InfoDisplayBase::fBusy = false;
  fMain->HandleAFL(fValue);
  InfoDisplayBase::fBusy = true;
}

//====================================================================
Init::Init(const char* dns)
{
  std::cout << "Setting DIM DNS NODE to " << dns << std::endl;
  DimClient::setDnsNode(dns);
}

//====================================================================
Main::Main(const char* dns, const char* name) 
  : TGMainFrame(gClient->GetRoot(), 300, 400, kVerticalFrame), 
    Init(dns),
    fName(name), 
    fLayouts(kLHintsExpandX, 3, 3, 0, 3),
    fTabs(this), 
    fFirst(fTabs.AddTab("Front")),
    fFront(fFirst),
    fMainState(&fFront, 0, Form("%s_MAIN_STATE", name), -1, 2, 1, 0), 
    fRcuState(&fFront, 0, Form("%s_RCU_STATE", name), -1, 2, 1, 0), 
    fAFL(&fFront, this, Form("%s_AFL", name)), 
    fERRST(&fFront, 0, Form("%s_ERRST", name), -1, 2, 1, 0),
    fFecs(32), 
    fFrames(32)
{
  DimClient::addErrorHandler(this);
  for (size_t i = 0; i < 32; i++) fFecs[i] = 0;
  AddFrame(&fTabs, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
  fFirst->AddFrame(&fFront,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
  fFront.AddFrame(&fMainState, &fLayouts);
  fFront.AddFrame(&fRcuState, &fLayouts);
  fFront.AddFrame(&fAFL, &fLayouts);
  fFront.AddFrame(&fERRST, &fLayouts);
  Resize(600, 400);
}

//____________________________________________________________________
void 
Main::CloseWindow()
{
  gApplication->Terminate();
}
//____________________________________________________________________
void 
Main::Display()
{
  SetWindowName("Monitor");
  MapSubwindows();
  Resize(600, 400);
  MapWindow();
  gApplication->Run();
}
//____________________________________________________________________
void 
Main::HandleAFL(int afl)
{
  LockGuard g(DimMutex::Instance());
  
  if (InfoDisplayBase::fBusy) return;
  static bool first = true;
  if (first) { 
    first = false;
    return;
  }

  gApplication->StartIdleing();
  InfoDisplayBase::fBusy = true;

  if (afl < 0 || afl > 0xFFFFFFFF) return;

  for (size_t i = 0; i < 32; i++) { 
    bool on = (afl & (1 << i));
    std::cout << (on ? '*' : '-');
    if (on) AddFec(i);
    else    RemoveFec(i);
  }
  std::cout << std::endl;
  InfoDisplayBase::fBusy = false;
  gApplication->StopIdleing();  
}
//____________________________________________________________________
void 
Main::AddFec(unsigned int addr) 
{ 
  if (fFecs[addr]) return;
  fFrames[addr] = fTabs.AddTab(Form("FMDD_0x%02x", addr));
  // fFrames[addr]->Resize(GetWidth()-10, GetHeight()-30);
  fFecs[addr]   = new Fec(fName.c_str(), addr, fFrames[addr]);
  fFrames[addr]->AddFrame(fFecs[addr]);
  fTabs.MapSubwindows();
  // fTabs.GetLayoutManager()->Layout();
  // gClient->NeedRedraw(this, kTRUE);
}

//____________________________________________________________________
void 
Main::RemoveFec(unsigned int addr)  {
  if (!fFecs[addr] && !fFrames[addr]) return;
  // std::cout << "Removing Fec " << addr << " " << fFecs[addr] << std::endl;
#if 0
  for (size_t i = 0; i < fTabs.GetNumberOfTabs(); i++) { 
    if (fTabs.GetTabContainer(i) == fFrames[addr]) {
      fTabs.RemoveTab(i);
      delete fFecs[addr];
      fFecs[addr] = 0;
      fFrames[addr] = 0;
      gClient->NeedRedraw(&fTabs, kTRUE);
      break;
      fTabs.MapSubwindows();
      fTabs.GetLayoutManager()->Layout();
    }
  }
#endif
  gClient->NeedRedraw(this, kTRUE);
}

//____________________________________________________________________
void 
Main::errorHandler(int severity, int code, char *msg)
{
  int index = 0;
  char** services = DimClient::getServerServices();
  std::cout << severity << " " << msg << "\n"
	    << "from "<< DimClient::getServerName() 
	      << " services:" << std::endl;
  while(services[index]) {
    std::cout << "\t" << services[index] << std::endl;
    index++;
  }
}

//____________________________________________________________________
Bool_t 
Main::HandleConfigureNotify(Event_t *event)
{
  Bool_t ret = TGMainFrame::HandleConfigureNotify(event);
  if (!IsMapped()) return ret;
  for (size_t i = 0; i < fTabs.GetNumberOfTabs(); i++) { 
    TGCompositeFrame* f   = fTabs.GetTabContainer(i);
    if (!f) { std::cout << " No container at " << i << std::endl; continue; }

    Fec* fec = 0;
    for (size_t no = 0; no < fFrames.size(); no++) 
      if (f == fFrames[no]) fec = fFecs[no];
    

    if (!fec) continue;

    fec->Resize(event->fWidth, event->fHeight-20);
    fec->fCanvas.Resize(event->fWidth-10, event->fHeight-30);
    fec->fCont.Resize(event->fWidth-10, event->fHeight-30);
    fec->Layout();
    fec->MapSubwindows();
    gClient->NeedRedraw(fec, kTRUE);
  }
  for (size_t no = 0; no < fFrames.size(); no++) {
    if (!fFrames[no]) continue;
    fFrames[no]->Layout();
  }
  return ret;
  // gClient->NeedRedraw(this, kTRUE);
}
  

//____________________________________________________________________
//
// EOF
//

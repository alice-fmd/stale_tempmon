#include "Lock.h"
#include <dic.hxx>

DimMutex* DimMutex::fgInstance = new DimMutex();
DimMutex& DimMutex::Instance()
{
  return *fgInstance;
}
int DimMutex::Lock()
{
  dim_lock();
  return 0;
}
int DimMutex::Unlock()
{
  dim_unlock();
  return 0;
}

#ifndef _WIN32
#include <pthread.h>
struct PosixLock : public Mutex
{

  PosixLock() { pthread_mutex_init(&_mutex, 0); }
  ~PosixLock() { pthread_mutex_destroy(&_mutex); }
  int Lock() { return pthread_mutex_lock(&_mutex); }
  int Unlock() { return pthread_mutex_unlock(&_mutex); }
  pthread_mutex_t _mutex;
};
Mutex* Mutex::Create() { return new PosixLock(); }
#else
#include <windows.h>
struct WinLock : public Mutex
{
  WinLock() { InitializeCriticalSection(&_critical); }
  ~WinLock() { DeleteCriticalSection(&_critical); }
  int Lock() { EnterCriticalSection(&_critical); return 0; }
  int UnLock() { LeaveCriticalSection(&_critical); return 0; }
  CRITICAL_SECTION _critical;
};
Mutex* Mutex::Create() { return new WinLock(); }
#endif
//
// EOF
//


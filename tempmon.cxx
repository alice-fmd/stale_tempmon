#include <iostream>
#include <string>
#include "Main.h"
#include "Traits.h"
#include "GraphTrait.h"
#include <TApplication.h>

//____________________________________________________________________
int main(int argc, char** argv)
{
  std::string dnsNode = "localhost";
  std::string name    = "FMD-FEE_0_0_0";
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') { 
      switch (argv[i][1]) { 
      case 'd': dnsNode = argv[++i]; break;
      case 'n': name    = argv[++i]; break;
      case 'h': 
	std::cout << "Usage: " << argv[0] << " [-d DNS] [-n NAME]" 
		  << std::endl;
	return 0;
      }
    }
  }

  TApplication app(Form("%s_client", name.c_str()), 0, 0);
  Main         m(dnsNode.c_str(), name.c_str());
  m.Display();
  
  return 0;
}

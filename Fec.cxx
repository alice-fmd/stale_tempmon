#include "Fec.h"

//____________________________________________________________________
Fec::Fec(const char* name, unsigned int addr, TGCompositeFrame* p) 
  : TGVerticalFrame(p, p->GetWidth(), p->GetHeight()),
    fAddress(addr),
    fCanvas(this, p->GetWidth(), p->GetHeight()-30, kChildFrame),
    fCont(fCanvas.GetViewPort(), p->GetWidth(), p->GetHeight()-30),
    fCSR1(&fCont, fAddress, Form("%s_%02d_CSR1", name, addr), -1, 1, 1),
    // Temps
    fT1(&fCont, fAddress, Form("%s_%02d_T1", name, addr), 
	-1, 1, 1, 0, int(1024./4+.5), kRed, .25),
    fT2(&fCont, fAddress, Form("%s_%02d_T2", name, addr), 
	-1, 1, 1, 0, int(1024./4+.5), kGreen, .25),
    fT3(&fCont, fAddress, Form("%s_%02d_T3", name, addr),
	-1, 1, 1, 0, int(1024./4+.5), kBlue, .25),
    fT4(&fCont, fAddress, Form("%s_%02d_T4", name, addr),
	-1, 1, 1, 0, int(1024./4+.5), kYellow, .25),
    fT1_SENS(&fCont, fAddress, Form("%s_%02d_T1_SENS", name, addr),
	     -1, 1, 1, 0, int(1024./8+.5), kMagenta, .125),
    fT2_SENS(&fCont, fAddress, Form("%s_%02d_T2_SENS", name, addr),
	     -1, 1, 1, 0, int(1024./8+.5), kCyan, .125),
    // 
    fFLASH_I(&fCont, fAddress, Form("%s_%02d_FLASH_I", name, addr), 
	     -1, 1, 1, 0, int(1024*1+.5), kBlue, 1.),
    fGTL_U(&fCont, fAddress, Form("%s_%02d_GTL_U", name, addr),
	   -1, 1, 1, 0, 5, kRed, 2.5/512),
    //
    fAL_DIG_I(&fCont, fAddress, Form("%s_%02d_AL_DIG_I", name, addr), 
	      -1, 1, 1, 0, int(1024*1+.5), kGreen, 1.),
    fAL_DIG_U(&fCont, fAddress, Form("%s_%02d_AL_DIG_U", name, addr),
	      -1, 1, 1, 0, 5, kCyan, 2.5/512),
    // 
    fAL_ANA_I(&fCont, fAddress, Form("%s_%02d_AL_ANA_I", name, addr), 
	      -1, 1, 1, 0, int(1024*1+.5), kMagenta, 1.),
    fAL_ANA_U(&fCont, fAddress, Form("%s_%02d_AL_ANA_U", name, addr),
	      -1, 1, 1, 0, 5, kYellow, 2.5/512),
    //
    fVA_REC_IP(&fCont, fAddress, Form("%s_%02d_VA_REC_IP", name, addr), 
	       -1, 1, 1, 0, int(1024*1+.5), kRed, 1.),
    fVA_REC_UP(&fCont, fAddress, Form("%s_%02d_VA_REC_UP", name, addr),
	       -1, 1, 1, 0, 5, kGreen, 2.5/512),
    //
    fVA_SUP_IP(&fCont, fAddress, Form("%s_%02d_VA_SUP_IP", name, addr), 
	       -1, 1, 1, 0, int(1024*1+.5), kBlue, 1.),
    fVA_SUP_UP(&fCont, fAddress, Form("%s_%02d_VA_SUP_UP", name, addr),
	       -1, 1, 1, 0, 3, kYellow, 1.5/512),
    fVA_SUP_IM(&fCont, fAddress, Form("%s_%02d_VA_SUP_IM", name, addr),
	       -1, 1, 1, 0, int(1024*1+.5), kMagenta, 1.),
    fVA_SUP_UM(&fCont, fAddress, Form("%s_%02d_VA_SUP_UM", name, addr),
	       -1, 1, 1, -4, 1, kCyan, 4.5/512, -2.5),
    //
    fVA_REC_IM(&fCont, fAddress, Form("%s_%02d_VA_REC_IM", name, addr),
	       -1, 1, 1, 0, int(1024*1+.5), kRed, 1.),
    fVA_REC_UM(&fCont, fAddress, Form("%s_%02d_VA_REC_UM", name, addr),
	       -1, 1, 1, -4, 1, kGreen, 4.5/512, -2.5),
    fHints(kLHintsExpandX, 0, 0, 1, 1)
{
  fCanvas.SetContainer(&fCont);
  fCont.AddFrame(&fCSR1, 	&fHints);

  fCont.AddFrame(&fT1,		&fHints);
  fCont.AddFrame(&fT2,		&fHints);
  fCont.AddFrame(&fT3,		&fHints);
  fCont.AddFrame(&fT4,		&fHints);
  fCont.AddFrame(&fT1_SENS,	&fHints);
  fCont.AddFrame(&fT2_SENS,	&fHints);

  fCont.AddFrame(&fFLASH_I,	&fHints);
  fCont.AddFrame(&fGTL_U,	&fHints);

  fCont.AddFrame(&fAL_DIG_I,	&fHints);
  fCont.AddFrame(&fAL_DIG_U,	&fHints);

  fCont.AddFrame(&fAL_ANA_I,	&fHints);
  fCont.AddFrame(&fAL_ANA_U,	&fHints);

  fCont.AddFrame(&fVA_REC_IP,	&fHints);
  fCont.AddFrame(&fVA_REC_UP,	&fHints);

  fCont.AddFrame(&fVA_REC_IM,	&fHints);
  fCont.AddFrame(&fVA_REC_UM,	&fHints);

  fCont.AddFrame(&fVA_SUP_IP,	&fHints);
  fCont.AddFrame(&fVA_SUP_UP,	&fHints);

  fCont.AddFrame(&fVA_SUP_IM,	&fHints);
  fCont.AddFrame(&fVA_SUP_UM,	&fHints);

#define GET_HIST(F) static_cast<GraphFrame*>((F).Frame())->fGraph.GetHistogram()
  GET_HIST(fT1)->SetYTitle("T [C]");
  GET_HIST(fT2)->SetYTitle("T [C]");
  GET_HIST(fT3)->SetYTitle("T [C]");
  GET_HIST(fT4)->SetYTitle("T [C]");
  GET_HIST(fT1_SENS)->SetYTitle("T [C]");
  GET_HIST(fT2_SENS)->SetYTitle("T [C]");
  GET_HIST(fFLASH_I)->SetYTitle("I [A]");
  GET_HIST(fGTL_U)->SetYTitle("U [V]");

  GET_HIST(fAL_DIG_I)->SetYTitle("I [A]");
  GET_HIST(fAL_DIG_U)->SetYTitle("U [V]");

  GET_HIST(fAL_ANA_I)->SetYTitle("I [A]");
  GET_HIST(fAL_ANA_U)->SetYTitle("U [V]");

  GET_HIST(fVA_REC_IP)->SetYTitle("I [A]");
  GET_HIST(fVA_REC_UP)->SetYTitle("U [V]");

  GET_HIST(fVA_REC_IM)->SetYTitle("I [A]");
  GET_HIST(fVA_REC_UM)->SetYTitle("U [V]");

  GET_HIST(fVA_SUP_IP)->SetYTitle("I [A]");
  GET_HIST(fVA_SUP_UP)->SetYTitle("U [V]");

  GET_HIST(fVA_SUP_IM)->SetYTitle("I [A]");
  GET_HIST(fVA_SUP_UM)->SetYTitle("U [V]");

  AddFrame(&fCanvas, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY, 3, 3));
}
//____________________________________________________________________
//
// EOF
//

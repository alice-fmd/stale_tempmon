#include "InfoDisplay.h"
#include <iostream>

//____________________________________________________________________
bool InfoDisplayBase::fBusy = false;

//____________________________________________________________________
InfoDisplayBase::InfoDisplayBase(TGCompositeFrame* p, 
				 unsigned int addr, 
				 const char* name) 
  : TGGroupFrame(p, name, kVerticalFrame), 
    fAddress(addr),
    fParent(p),
    fName(name)
    // fStatus(this, "No link")
  {
    fBusy = true;
    // AddFrame(&fStatus, new TGLayoutHints(kLHintsExpandX, 3, 3, 3, 3));
    // Resize(p->GetWidth()-10,p->GetHeight()-10);
  }
//____________________________________________________________________
void 
InfoDisplayBase::SetStatus(const char* what) 
{ 
  // fStatus.SetText(what);
  SetTitle(Form("%s - %s", fName.c_str(), what));
  // gClient->NeedRedraw(&fStatus, kTRUE);
  // gClient->NeedRedraw(this, kTRUE);
  DoRedraw();
}
//____________________________________________________________________
//
// EOF
//
